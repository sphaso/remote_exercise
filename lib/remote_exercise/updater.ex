defmodule RemoteExercise.Updater do
  use Oban.Worker,
    queue: :update_users

  require Logger
  alias RemoteExercise.State

  @impl Oban.Worker
  def perform(%Oban.Job{}) do
    Logger.info("Invoked Users update job")
    State.update()
  end
end
