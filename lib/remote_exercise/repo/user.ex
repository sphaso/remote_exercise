defmodule RemoteExercise.Repo.User do

  use Ecto.Schema
  import Ecto.Query
  alias RemoteExercise.Repo

  schema "users" do
    field :points, :integer, default: 0
    field :inserted_at, :utc_datetime_usec, default: DateTime.utc_now()
    field :updated_at, :utc_datetime_usec, default: DateTime.utc_now()
  end

  def update(min, max) do
    statement = "UPDATE users SET points = floor(random() * #{max-min+1} + #{min})::int;"
    Ecto.Adapters.SQL.query!(RemoteExercise.Repo, statement, [])
  end

  def get_above(threshold) do
    Repo.all(from p in __MODULE__, where: p.points > ^threshold, limit: 2)
  end

end
