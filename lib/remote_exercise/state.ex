defmodule RemoteExercise.State do

  use GenServer

  alias RemoteExercise.Repo.User

  @min 0
  @max 100

  def start_link(arg) do
    GenServer.start_link(__MODULE__, arg, name: __MODULE__)
  end

  @impl true
  def init(_) do
    {:ok, %{min_number: random(), timestamp: nil}}
  end

  @impl true
  def handle_cast(:update, state) do
    state = %{state | min_number: random()}
    User.update(@min, @max)
    {:noreply, state}
  end

  @impl true
  def handle_call(:get, _from, state) do
    new_state = %{state | timestamp: DateTime.utc_now()}
    users = User.get_above(state.min_number)
    {:reply, {users, state.timestamp}, new_state}
  end

  def update() do
    GenServer.cast(__MODULE__, :update)
  end

  def get() do
    GenServer.call(__MODULE__, :get)
  end

  defp random() do
    Enum.random(@min..@max)
  end
end
