defmodule RemoteExerciseWeb.PageController do
  use RemoteExerciseWeb, :controller

  def index(conn, _params) do
    {users, previous_timestamp} = RemoteExercise.State.get()
    users = Enum.map(users, &Map.from_struct/1)
    timestamp = if is_nil(previous_timestamp), do: nil, else: DateTime.truncate(previous_timestamp, :second)
    render(conn, "show.json", users: users, timestamp: timestamp)
  end
end
