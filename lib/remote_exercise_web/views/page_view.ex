defmodule RemoteExerciseWeb.PageView do
  use RemoteExerciseWeb, :view

  def render("show.json", %{users: users, timestamp: timestamp}) do
   %{
      timestamp: timestamp,
      users: render_many(users, __MODULE__, "user.json")
    }
  end

  def render("user.json", %{page: %{id: id, points: points}}) do
    %{id: id, points: points}
  end
end
