# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     RemoteExercise.Repo.insert!(%RemoteExercise.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

statement = "INSERT INTO users (inserted_at, updated_at) SELECT NOW(), NOW() FROM generate_series(1, 1000000);"
Ecto.Adapters.SQL.query!(RemoteExercise.Repo, statement, [])
