# RemoteExercise

RemoteExercise is a web application which:
- returns two `User`s and a timestamp when called on `GET /`
- updates its `User`s' score at regular intervals

## Internals

### Users
`User`s are stored in a single Postgres table
```
   Column    |            Type             | Collation | Nullable |              Default              | Storage | Compression | Stats target | Description
-------------+-----------------------------+-----------+----------+-----------------------------------+---------+-------------+--------------+-------------
 id          | bigint                      |           | not null | nextval('users_id_seq'::regclass) | plain   |             |              |
 points      | integer                     |           | not null | 0                                 | plain   |             |              |
 inserted_at | timestamp without time zone |           | not null |                                   | plain   |             |              |
 updated_at  | timestamp without time zone |           | not null |                                   | plain   |             |              |
Indexes:
    "users_pkey" PRIMARY KEY, btree (id)
```

`points` defaults to 0 and range between 0 and 100. Please note that these bounds are defined in `RemoteExercise.State`

`inserted_at` and `updated_at` are to be considered UTC times

At startup, 1_000_000 default `User`s are created.

### Updates
An `Oban` job is run every minute. This job will call `State.update()` which in turn will:
- update its internal field `min_rand` to a random number between `@min` and `@max`
- call another function to update each `User` `points` field to a random number

### Get
`State` provides a function, `get`, to retrieve up to two `User`s whose `points` are above `min_rand`.

On each call, an internal `timestamp` will be updated.

The previous `timestamp` and two `User`s conforming to the criteria above are returned when issuing a `GET` request on `/`.

## Running

### Docker
The app has been dockerized for your convenience.
```
docker compose up --build
```

will expose the Phoenix app on localhost:4000.

In order to run further commands,
```
docker compose exex remote_exercise bash
```
will allow you to enter the running container.

### Local
If Docker is not desirable or available, you can run the usual `mix` commands from your local machine.
We should then assume that Postgres is running with default configuration.
