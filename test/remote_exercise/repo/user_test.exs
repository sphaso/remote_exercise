defmodule RemoteExercise.Repo.UserTest do
  use RemoteExercise.DataCase

  alias RemoteExercise.Repo
  alias RemoteExercise.Repo.User

  test "Numbers change on each update" do
    Repo.insert(%User{points: 50})
    User.update(51, 100)

    %User{points: points} = Repo.one!(User)

    assert points != 50
  end

  test "get_above returns up to two users" do
    Repo.insert(%User{points: 50})
    Repo.insert(%User{points: 50})
    Repo.insert(%User{points: 50})
    users = User.get_above(0)

    assert length(users) == 2
  end

  test "get_above returns users whose points are above a certain threshold" do
    Repo.insert(%User{points: 10})
    Repo.insert(%User{points: 20})
    Repo.insert(%User{points: 30})
    users = User.get_above(18)

    assert Enum.all?(users, &(&1.points > 18))
  end

end
