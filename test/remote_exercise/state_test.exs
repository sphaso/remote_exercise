defmodule RemoteExercise.StateTest do
  use RemoteExercise.DataCase
  alias RemoteExercise.State

  test "get updates internal timestamp" do
    # avoid case of first call returning nil
    _ = State.get()
    {_, first} = State.get()
    {_, second} = State.get()

    assert DateTime.compare(first, second) == :lt
  end
end
