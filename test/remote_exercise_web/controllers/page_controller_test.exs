defmodule RemoteExerciseWeb.PageControllerTest do
  use RemoteExerciseWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    response = json_response(conn, 200)
    assert Map.keys(response) == ["timestamp", "users"]
  end
end
